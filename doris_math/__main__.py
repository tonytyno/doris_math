import sys
from doris_math.math_destroyer import loop_in_range
from doris_math.doc_creator import write_file, show_doc


def main(mode: str, examples: int, limit: int) -> int:
    print("** hello! **")
    print(f"mode set to: {mode}, creating {examples} examples with limit to {limit}")

    title_job, output_list = loop_in_range(mode, examples, limit)
    print("examples created, now goind to write a docx file")
    document = write_file(mode, examples, limit, title_job, output_list)
    show_doc(document)

    print("** everything worked perfectly, going to sleep **")
    return 0


if __name__ == "__main__":
    mode = sys.argv[1]
    examples = int(sys.argv[2])
    limit = int(sys.argv[3])
    main(mode, examples, limit)
