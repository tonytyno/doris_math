import subprocess

from docx import Document
from pathlib import Path
from docx.shared import Pt
from datetime import datetime

BASE = (Path(__file__) / "../..").resolve()


def write_file(
    mode: str, examples: int, limit: int, title_job: str, output_list: list
) -> Path:
    document = Document()
    title = f"Dorotko {title_job} te liczby!"
    document.add_heading(title, 0)

    document.add_paragraph(
        f"Przed tob\u0105 {examples} przyk\u0142ad\u00f3w w zakresie do {limit} "
    )

    style = document.styles["Normal"]
    font = style.font
    font.name = "Arial"
    font.size = Pt(16)
    style.paragraph_format.line_spacing = 1.5

    for equation in output_list:
        document.add_paragraph(equation, style="List Bullet")

    table = document.add_table(rows=2, cols=3)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = "Liczba zada\u0144"
    hdr_cells[1].text = "Poprawnych"
    hdr_cells[2].text = "B\u0142\u0119dnych"
    second_row = table.rows[1].cells
    second_row[0].text = str(examples)

    now = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
    (BASE / "output_files").mkdir(parents=True, exist_ok=True)
    doc_path = (
        BASE / f"output_files/{mode}_{examples}_examples_{limit}_limit_{now}.docx"
    )
    document.save(doc_path)
    return doc_path


def show_doc(document_path: Path) -> int:
    run = subprocess.run(
        ["libreoffice", document_path],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        text=True,
    )

    if run.returncode != 0:
        print(f"Not able to open file {document_path}")
        return 1
    return 0
