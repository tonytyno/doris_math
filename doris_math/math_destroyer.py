from random import randrange


def create_addition(limit: int) -> str:
    if limit <= 0:
        raise Exception("Please set limit")
    first_arg = randrange(int(limit / 2))
    second_arg = randrange(int(limit / 2))
    return f"{first_arg} + {second_arg} ="


def create_subtraction(limit: int) -> str:
    if limit <= 0:
        raise Exception("Please set limit")
    first_arg = randrange(int(limit / 2))
    second_arg = randrange(int(limit / 2))
    if first_arg < second_arg:
        return create_subtraction(limit)
    return f"{first_arg} - {second_arg} ="


def create_multiplication(limit: int) -> str:
    if limit <= 0:
        raise Exception("Please set limit")
    first_arg = randrange(int(limit))
    second_arg = randrange(int(limit))
    return f"{first_arg} * {second_arg} ="


def create_division(limit: int) -> str:
    if limit <= 0:
        raise Exception("Please set limit")
    first_arg = randrange(int(limit))
    second_arg = randrange(int(limit))
    if second_arg == 0:
        return create_division(limit)
    return f"{first_arg} : {second_arg} ="


def loop_in_range(mode: str, examples: int, limit: int) -> (str, list):
    output = list

    match mode:
        case "add":
            title_job = "dodaj"
            output = [create_addition(limit) for x in range(examples)]
        case "subtract":
            title_job = "odejmij"
            output = [create_subtraction(limit) for x in range(examples)]
        case "multiply":
            title_job = "pomn\u00f3\u017c"
            output = [create_multiplication(limit) for x in range(examples)]
        case "divide":
            title_job = "podziel"
            output = [create_division(limit) for x in range(examples)]
        case _:
            raise Exception(
                "Please select proper mode from [add, subtract, multiply, divide] !"
            )

    return title_job, output
